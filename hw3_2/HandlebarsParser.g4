parser grammar HandlebarsParser;

options { tokenVocab=HandlebarsLexer; }

document : element* EOF ;

element
    : rawElement
    | expressionElement
    | blockElement
    | commentElement
    ;

rawElement: TEXT (BRACE TEXT)* BRACE?;

expressionElement: START expression END;

expression
    : literals
    | lookups
    | helpers
    | OPEN_PAREN expression CLOSE_PAREN;

literals
    : INTEGER  #IntegerLiteral
    | FLOAT    #FloatLiteral
    | STRING   #StringLiteral
    ;

lookups: ID;

helpers: ID expression+;

blockElement: openBlockElement element* closeBlockElement;

openBlockElement: START openBlock expression* END;

openBlock: BLOCK id = ID;

closeBlockElement: START closeBlock END;

closeBlock: CLOSE_BLOCK id = ID;

commentElement : START COMMENT END_COMMENT ;
