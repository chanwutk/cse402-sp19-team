const antlr4 = require("antlr4");
const HandlebarsLexer = require("../gen/HandlebarsLexer").HandlebarsLexer;
const HandlebarsParser = require("../gen/HandlebarsParser").HandlebarsParser;
const HandlebarsParserListener = require("../gen/HandlebarsParserListener").HandlebarsParserListener;

function escapeString(s) {
    return ("" + s).replace(/["'\\\n\r\u2028\u2029]/g, (c) => {
        switch (c) {
            case '"':
            case "'":
            case "\\":
                return "\\" + c;
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\u2028":
                return "\\u2028";
            case "\u2029":
                return "\\u2029";
        }
    });
}

function separateSignAndNumber(text) {
    let sign = 1;
    switch (text.charAt(0)) {
        case '-':
            sign = -1;
        case '+':
            text = text.substring(1);
    }
    return { sign, number: text };
}

class HandlebarsCompiler extends HandlebarsParserListener {
    constructor() {
        super();
        this._inputVar = "_$ctx";
        this._outputVar = "_$result";
        this._helpers = { expr: {}, block: {} };

        this.registerBlockHelper('each', (ctx, body, param) => {
            let result = '';
            param.forEach(elm => result += body(elm));
            return result;
        });

        this.registerBlockHelper('if', (ctx, body, param) => param ? body(ctx) : '');

        this.registerBlockHelper('with', (ctx, body, param) => body(ctx[param]));
    }

    registerExprHelper(name, helper) {
        this._helpers.expr[name] = helper;
    }

    registerBlockHelper(name, helper) {
        this._helpers.block[name] = helper;
    }

    compile(template) {
        this.usedExpr = new Set();
        this.usedBlock = new Set();
        this.exprStack = [];
        this.blockStack = [];
        this._bodyStack = [''];
        this.pushScope();

        const chars = new antlr4.InputStream(template);
        const lexer = new HandlebarsLexer(chars);
        const tokens = new antlr4.CommonTokenStream(lexer);
        const parser = new HandlebarsParser(tokens);
        parser.buildParseTrees = true;
        const tree = parser.document();
        antlr4.tree.ParseTreeWalker.DEFAULT.walk(this, tree);

        this.popScope();
        this.appendToTopScope(`return ${this._outputVar};\n`);

        let funcs = '';
        this.usedBlock.forEach(b => funcs += `const __$${b} = ${this._helpers.block[b].toString()};\n`);
        this.usedExpr.forEach(e => funcs += `const __$${e} = ${this._helpers.expr[e].toString()};\n`);
        return new Function(this._inputVar, funcs + this._bodyStack[0]);
    }

    appendToTopScope(text) {
        this._bodyStack[this._bodyStack.length - 1] += text;
    }

    pushScope() {
        this._bodyStack.push(`var ${this._outputVar} = "";\n`);
    }

    popScope() {
        this.appendToTopScope(this._bodyStack.pop());
    }

    append(expr) {
        this.appendToTopScope(`${this._outputVar} += ${expr};\n`);
    }

    exitRawElement(ctx) {
        this.append(`"${escapeString(ctx.getText())}"`);
    }

    enterStringLiteral(ctx) {
        this.exprStack.push(ctx.getText());
    }

    enterFloatLiteral(ctx) {
        const { sign, number } = separateSignAndNumber(ctx.getText());
        const float = parseFloat(number);
        this.exprStack.push(`${sign === -1 ? '-' : ''}${float}${float === 0 ? '.' : ''}`);
    }

    enterIntegerLiteral(ctx) {
        let { sign, number } = separateSignAndNumber(ctx.getText());
        let base = 10;
        if (number.length > 1 && number.charAt(0) === '0') {
            const prefix = number.substring(0, 2);
            switch (prefix) {
                case '0x':
                case '0X':
                    base = 16;
                    break;
                case '0B':
                case '0b':
                    base = 2;
                    break;
                default:
                    base = 8;
            }
            if (isNaN(prefix.charAt(1))) {
                number = number.substring(2);
            }
        }
        this.exprStack.push(`${sign * parseInt(number, base)}`);
    }

    exitLookups(ctx) {
        this.exprStack.push(`${this._inputVar}.${ctx.getText()}`);
    }

    enterHelpers(ctx) {
        const start = ctx.start;
        const expr = start.source[0]._input.strdata.substring(start.start, start.stop + 1);
        if (!this.usedExpr.has(expr)) {
            this.usedExpr.add(expr);
        }
        this.exprStack.push(expr);
    }

    exitHelpers(ctx) {
        const n = ctx.children.length;
        let params = Array(n - 1);
        for (let i = n - 2; i >= 0; i--) {
            params[i] = this.exprStack.pop();
        }
        const expr = this.exprStack.pop();
        this.exprStack.push(`__$${expr} (${this._inputVar}, ...[${params.toString()}])`);
    }

    exitExpressionElement(ctx) {
        const expr = this.exprStack.pop();
        this.append(expr === '-0.' ? `'-0'` : expr);
    }

    enterOpenBlock(ctx) {
        this.blockStack.push(ctx.getText().substring(1));
    }

    exitOpenBlockElement(ctx) {
        this.pushScope();
    }

    enterCloseBlock(ctx) {
        const block = ctx.getText().substring(1);
        if (block !== this.blockStack.pop()) {
            throw new Error("Block start 'block' does not match the block end 'wrong'.");
        }
        this.appendToTopScope(`return ${this._outputVar};\n`);
        const scope = new Function(this._inputVar, this._bodyStack[this._bodyStack.length - 1]);
        this._bodyStack[this._bodyStack.length - 1] = '';
        if (!this.usedBlock.has(block)) {
            this.usedBlock.add(block);
        }
        this.append(`__$${block}(${this._inputVar}, ${scope.toString()}, ${this.exprStack.pop()})`);
        this.popScope();
    }
}

exports.HandlebarsCompiler = HandlebarsCompiler;
