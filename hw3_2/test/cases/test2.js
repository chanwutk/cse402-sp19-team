function subtract(ctx, ...args) {
    return args.reduce((a, x) => a - x); 
}

exports.helpers = [
    ["subtract", subtract],
];
exports.blockHelpers = [];
exports.ctx = {};
exports.description = "Custom test 2 (Aye you can subtract too!)";
