function multiply(ctx, ...args) {
    return args.reduce((a, x) => a * x); 
}

exports.helpers = [
    ["multiply", multiply],
];
exports.blockHelpers = [];
exports.ctx = {};
exports.description = "Custom test 1 (Now, not even mupltiplacation is a headache!";
