function istrue(ctx, arg) {
  return arg === 'true';
} 

exports.helpers = [["istrue", istrue]];
exports.blockHelpers = [ ];
exports.ctx = {
  year: 2019,
  classes: [
    {
      cancle: 'true',
      name: 'Domain Specific Language',
      code: 402,
      time: [
        {
          day: 'Mon',
          start: '12:30pm',
          end: '1:30pm',
          cancle: 'false'
        },
        {
          day: 'Wed',
          start: '12:30pm',
          end: '1:30pm',
          cancle: 'true'
        },
        {
          day: 'Fri',
          start: '12:30pm',
          end: '1:30pm',
          cancle: 'false'
        }
      ]
    },
    {
      name: 'Compiler',
      code: 401,
      time: [
        {
          day: 'Mon',
          start: '8:30am',
          end: '9:30am',
          cancle: 'false'
        },
        {
          day: 'Wed',
          start: '8:30am',
          end: '9:30am',
          cancle: 'false'
        },
        {
          day: 'Fri',
          start: '8:30am',
          end: '9:30am',
          cancle: 'true'
        }
      ]
    }
  ]
};
exports.description = "Custom test 4! nested each and custom function";
 