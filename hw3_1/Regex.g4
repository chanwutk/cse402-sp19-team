grammar Regex;

re
    : alt EOF
    ;

alt
    : expression ('|' expression)*
    ;

expression
    : (elems+=element)*
    ;

element
    : a=atom            # AtomicElement
    | a=atom m=MODIFIER # ModifiedElement
    ;

atom
    : c=CHARACTER       # CharacterAtom
    | '(' alt ')'       # NestedAtom
    ;

CHARACTER : [a-zA-Z0-9.] ;
MODIFIER : [?*] ;
WS : [ \t\r\n]+ -> skip ;
