/* Q is a small query language for JavaScript.
 *
 * Q supports simple queries over a JSON-style data structure,
 * kinda like the functional version of LINQ in C#.
 *
 */

// This class represents all AST Nodes.
export class ASTNode {
  public readonly type: string;

  constructor(type: string) {
    this.type = type;
  }

  execute(data: any[]): any {
    throw new Error("Execute not implemented for " + this.type + " node.");
  }

  optimize(): ASTNode {
    return this;
  }

  run(data: any[]): any {
    return this.optimize().execute(data);
  }

  //// 1.5 implement call-chaining

  filter(predicate: (datum: any) => boolean): ASTNode {
    return new ThenNode(this, new FilterNode(predicate));
  }

  apply(callback: (datum: any) => any): ASTNode {
    return new ThenNode(this, new ApplyNode(callback));
  }

  count(): ASTNode {
    return new ThenNode(this, new CountNode());
  }

  product(query: ASTNode): ASTNode {
    return new CartesianProductNode(this, query);
  }

  join(
    query: ASTNode,
    relation: string | ((left: any, right: any) => any),
  ): ASTNode {
    if (typeof relation === "string") {
      return new HashJoinNode(relation, this, query);
    } else {
      return new JoinNode(relation, this, query);
    }
  }

  sort(compare: (a, b) => number): ASTNode {
    return new SortByNode(this, compare);
  }

  reduce(reducer: (accumulator, currentValue) => any, initial: any): ASTNode {
    return new ReduceNode(this, reducer, initial);
  }
}

// The Id node just outputs all records from input.
export class IdNode extends ASTNode {
  constructor() {
    super("Id");
  }

  //// 1.1 implement execute

  execute(data: any[]): any {
    return data;
  }
}

// We can use an Id node as a convenient starting point for
// the call-chaining interface.
export const Q = new IdNode();

// The Filter node uses a callback to throw out some records.
export class FilterNode extends ASTNode {
  predicate: (datum: any) => boolean;

  constructor(predicate: (datum: any) => boolean) {
    super("Filter");
    this.predicate = predicate;
  }

  //// 1.1 implement execute

  execute(data: any[]): any {
    return data.filter(this.predicate);
  }
}

// The Then node chains multiple actions on one data structure.
export class ThenNode extends ASTNode {
  first: ASTNode;
  second: ASTNode;

  constructor(first: ASTNode, second: ASTNode) {
    super("Then");
    this.first = first.optimize();
    this.second = second.optimize();
  }

  //// 1.1 implement execute

  optimize(): ASTNode {
    return new ThenNode(this.first, this.second);
  }

  execute(data: any[]): any {
    return this.second.execute(this.first.execute(data));
  }
}

//// 1.3 implement Apply and Count Nodes

export class ApplyNode extends ASTNode {
  callback: (datum: any) => any;

  constructor(callback: (datum: any) => any) {
    super("Apply");
    this.callback = callback;
  }

  execute(data: any[]): any {
    return data.map(this.callback);
  }
}

export class CountNode extends ASTNode {
  constructor() {
    super("Count");
  }

  execute(data: any[]): any {
    return [data.length];
  }
}

//// 2.1 optimize queries

// This function permanently adds a new optimization function to a node type.
// An optimization function takes in a node and either returns a new,
// optimized node or null if no optimizations can be performed. AddOptimization
// will register this function to a particular node type, so that it will be called
// as part of that node's optimize() method, along with all other registered
// optimizations.
function AddOptimization(nodeType, opt: (this: ASTNode) => ASTNode | null) {
  const oldOptimize = nodeType.prototype.optimize;
  nodeType.prototype.optimize = function (this: ASTNode): ASTNode {
    const newThis = oldOptimize.call(this);
    return opt.call(newThis) || newThis;
  };
}

// For example, the following small optimization removes unnecessary Id nodes.
AddOptimization(ThenNode, function (this: ThenNode): ASTNode {
  if (this.first instanceof IdNode) {
    return this.second;
  } else if (this.second instanceof IdNode) {
    return this.first;
  } else {
    return null;
  }
});

// The above optimization has a few notable side effects. First, it removes the
// root IdNode from your call chain, so if you were depending on that to exist
// your other optimizations may be confused. Also, it returns the interesting
// subtree directly, without trying to optimize it more. It is up to you to
// determine where additional optimization should occur.

// We won't specifically test for this optimization, so if it's giving you
// a headache feel free to comment it out.

// You can add optimizations for part 2.1 here (or anywhere below).
// ...

AddOptimization(ThenNode, function (this: ThenNode): ASTNode {
  const first = this.first;
  const second = this.second;
  if (second instanceof FilterNode) {
    if (first instanceof FilterNode) {
      return new FilterNode(
        (datum) => first.predicate(datum) && second.predicate(datum),
      );
    } else if (
      first instanceof ThenNode &&
      first.second instanceof FilterNode
    ) {
      return new ThenNode(
        first.first.optimize(),
        new FilterNode(
          (datum) =>
            (first.second as FilterNode).predicate(datum) &&
            second.predicate(datum),
        ),
      );
    }
  } else if (
    first instanceof FilterNode &&
    second instanceof ThenNode &&
    second.first instanceof FilterNode
  ) {
    return new ThenNode(
      new FilterNode(
        (datum) =>
          first.predicate(datum) &&
          (second.first as FilterNode).predicate(datum),
      ),
      second.second.optimize(),
    );
  }

  return this;
});

//// 2.2 internal node types and CountIf

AddOptimization(ThenNode, function (this: ThenNode): ASTNode {
  const first = this.first;
  const second = this.second;

  if (first instanceof FilterNode && second instanceof CountNode) {
    return new CountIfNode(first.predicate);
  } else if (
    first instanceof ThenNode &&
    first.second instanceof FilterNode &&
    second instanceof CountNode
  ) {
    return new ThenNode(
      first.first.optimize(),
      new CountIfNode((first.second.optimize() as FilterNode).predicate),
    );
  }

  return this;
});

export class CountIfNode extends ASTNode {
  predicate: (datum: any) => boolean;

  constructor(predicate: (datum: any) => boolean) {
    super("CountIf");
    this.predicate = predicate;
  }

  execute(data: any[]): any {
    return [data.filter(this.predicate).length];
  }
}

//// 3.1 cartesian products

export class CartesianProductNode extends ASTNode {
  left: ASTNode;
  right: ASTNode;

  constructor(left: ASTNode, right: ASTNode) {
    super("CartesianProduct");
    this.left = left.optimize();
    this.right = right.optimize();
  }

  execute(data: any[]): any {
    const leftArray = this.left.execute(data);
    const rightArray = this.right.execute(data);

    return [].concat(
      ...leftArray.map((left) => rightArray.map((right) => ({ left, right }))),
    );
  }
}

//// 3.2-3.6 joins and hash joins

/* Hint: Recall from the spec that a join of two arrays P and Q by a
   function f(l, r) is the array with one entry for each pair of a record
   in P and Q for which f returns true and that each entry should have all
   fields and values that either record in the pair had.

   Most notably, if both records had the same field, use the value from the
   record from Q. In JavaScript, this can be achieved with Object.assign:
   https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
*/

export class JoinNode extends ASTNode {
  left: ASTNode;
  right: ASTNode;
  predicate: (l: any, r: any) => boolean;
  constructor(predicate: (l: any, r: any) => boolean, left: ASTNode, right: ASTNode) {
    super("Join");
    this.left = left.optimize();
    this.right = right.optimize();
    this.predicate = predicate;
  }

  execute(data: any[]): any {
    const leftData = this.left.execute(data);
    const rightData = this.left.execute(data);
    const result = [];

    const pushJoined = leftData.length > 0 && Array.isArray(leftData[0]) ?
    (l, r) => result.push([...r, ...l]) :
    (l, r) => result.push({ ...r, ...l });

    for (const l of leftData) {
      for (const r of rightData) {
        if (this.predicate(l, r)) {
          pushJoined(l, r);
        }
      }
    }
    return result;
  }
}

/* Hint: While optimizing the fluent join to use your internal JoinNode,
   if you find yourself needing to compare two functions for equality,
   you can do so with the Javascript operator `===` (pointer equality).
 */

export class HashJoinNode extends ASTNode {
  joinField: string;
  left: ASTNode;
  right: ASTNode;
  constructor(joinField: string, left: ASTNode, right: ASTNode) {
    super("HashJoin");
    this.joinField = joinField;
    this.left = left.optimize();
    this.right = right.optimize();
  }

  execute(data: any[]): any {
    const leftDict = this.generateHashTable(this.left.execute(data));
    const rightDict = this.generateHashTable(this.right.execute(data));
    const result = [];
    for (const key of Object.keys(leftDict)) {
      for (const left of rightDict[key] || []) {
        for (const right of leftDict[key] || []) {
          result.push({ ...right, ...left });
        }
      }
    }
    return result;
  }

  private generateHashTable(data: any[]): any {
    const dict = {};
    for (const row of data) {
      if (!(row[this.joinField] in dict)) {
        dict[row[this.joinField]] = [];
      }
      dict[row[this.joinField]].push(row);
    }
    return dict;
  }
}

export class SortByNode extends ASTNode {
  query: ASTNode;
  compare: (a, b) => number;

  constructor(query: ASTNode, compare: (a, b) => number) {
    super("Sort");
    this.query = query.optimize();
    this.compare = compare;
  }

  execute(data: any[]): any {
    const exec = this.query.execute(data);
    exec.sort(this.compare);
    return exec;
  }
}

export class ReduceNode extends ASTNode {
  query: ASTNode;
  reducer: (accumulator, currentValue) => any;
  initial: any;

  constructor(query: ASTNode, reducer: (accumulator, currentValue) => any, initial: any) {
    super("Reduce");
    this.query = query.optimize();
    this.reducer = reducer;
    this.initial = initial;
  }

  execute(data: any[]): any {
    const exec = this.query.execute(data);
    exec.reduce(this.reducer, this.initial);
    return [exec.reduce(this.reducer, this.initial)];
  }
}
