import * as q from "./q";
const Q = q.Q;

//// 1.2 write a query

export const theftsQuery = new q.FilterNode((datum) => datum[2].match(/THEFT/));
export const autoTheftsQuery = new q.FilterNode((datum) => datum[3] === "MOTOR VEHICLE THEFT");

//// 1.4 clean the data

export const cleanupQuery = new q.ApplyNode((datum) => ({ description: datum[2], category: datum[3], area: datum[4], date: datum[5] }));

//// 1.6 reimplement queries with call-chaining

export const cleanupQuery2 = Q.apply((datum) => ({ description: datum[2], category: datum[3], area: datum[4], date: datum[5] }));
export const theftsQuery2 = Q.filter((datum) => datum.description.match(/THEFT/));
export const autoTheftsQuery2 = Q.filter((datum) => datum.category === "MOTOR VEHICLE THEFT");

//// 4 put your queries here (remember to export them for use in tests)

// utilities
export const secondTable = Q.apply((datum) => ({ id2: datum[0], description2: datum[2], category2: datum[3], area2: datum[4], date2: datum[5] }));
const filterOutTojoin = (datum) => {
  const {tojoin, ...result} = datum;
  return result;
};

// query 1
export const summerCrimeQuery = cleanupQuery2.apply((datum) => ({description: datum.description, month: datum.date.split("-")[1]}))
  .filter((datum) => 5 <= parseInt(datum.month, 10) && parseInt(datum.month, 10) <= 7)
  .apply((datum) => ({description: datum.description}));

// query 2
export const numOfCrimeInParkQuery = cleanupQuery2.filter((datum) => datum.area.match(/PARK/)).count();

// query 3
export const pairOfSameAreaQuery = cleanupQuery2
  .apply((datum) => ({...datum, tojoin: datum.area}))
  .join(secondTable.apply((datum) => ({...datum, tojoin: datum.area2})), "tojoin")
  .filter((datum) => datum.id < datum.id2)
  .apply(filterOutTojoin);

// query 4
const crimeInDec = cleanupQuery2.filter((datum) => datum.date.split("-")[1] === "12").apply((datum) => ({...datum, tojoin: datum.area}));
const crimeInJan = secondTable.filter((datum) => datum.date2.split("-")[1] === "01").apply((datum) => ({...datum, tojoin: datum.area2}));
export const commonCrimeAreaInJanAndDecQuery = crimeInJan.join(crimeInDec, "tojoin").apply(filterOutTojoin);

// query 5
export const duplicateTuplesQuery = Q.join(Q, (l, r) => true).apply((datum) => datum.slice(0, datum.length / 2));
