import {expect} from "chai";

import * as q from "../src/q";
const Q = q.Q;
import * as queries from "../src/queries";
import {crimeData} from "./data";
const rawData = crimeData.data;

// You should provide at least 5 tests, using your own queries from Part 4
// and either the crime data from test/data.ts or your own data. You can look
// at test/q.ts to see how to import and use the queries and data.
// The outline of the first test has been provided as an example - you should
// modify it so it actually does something interesting.

function flattenThenNodes(query) {
    if (query instanceof q.ThenNode) {
        return flattenThenNodes(query.first).concat(flattenThenNodes(query.second));
    } else if (query instanceof q.IdNode) {
        return [];
    } else {
        return [query];
    }
}

function expectQuerySequence(query, seq) {
    const flat = flattenThenNodes(query);
    expect(flat.length).to.be.equal(seq.length);
    for (let i = 0; i < flat.length; i++) {
        expect(flat[i].type).to.be.equal(seq[i]);
    }
}

function cleanData() {
    const out = [];
    for (const x of rawData) {
        out.push({description: x[2], category: x[3], area: x[4], date: x[5]});
    }
    return out;
}

describe("custom", () => {

    it("put description of test 1 here", () => {
        const query = Q;
        expect(query.type).to.be.equal("Id");
    });

    it("produces correct output for summerCrimeQuery", () => {
        const out = queries.summerCrimeQuery.execute(rawData);
        const filtered = [];

        for (const datum of queries.cleanupQuery2.execute(rawData)) {
            const month = datum.date.split("-")[1];
            if (5 <= parseInt(month, 10) && parseInt(month, 10) <= 7) {
                filtered.push({description: datum.description});
            }
        }

        expect(out).to.deep.equal(filtered);
    });

    it("produces correct output for numOfCrimeInParkQuery", () => {
        const out = queries.numOfCrimeInParkQuery.execute(rawData);
        let count = 0;

        for (const datum of queries.cleanupQuery2.execute(rawData)) {
            if (datum.area.match(/PARK/)) {
                count++;
            }
        }

        expect(out[0]).to.equal(count);
    });

    it("produces correct output for pairOfSameAreaQuery", () => {
        const out = queries.pairOfSameAreaQuery.execute(rawData);
        const cleaned1 = queries.cleanupQuery2.execute(rawData);
        const cleaned2 = queries.secondTable.execute(rawData);
        const filtered = [];

        for (const datum1 of cleaned1) {
            for (const datum2 of cleaned2) {
                if (datum1.area === datum2.area2 && datum1.id < datum2.id2) {
                    filtered.push({...datum1, ...datum2});
                }
            }
        }

        expect(out).to.deep.equal(filtered);
    });

    it("produces correct output for commonCrimeAreaInJanAndDecQuery", () => {
        const out = queries.commonCrimeAreaInJanAndDecQuery.execute(rawData);
        const cleaned1 = queries.cleanupQuery2.execute(rawData);
        const cleaned2 = queries.secondTable.execute(rawData);
        const crimeInJan = [];
        const crimeInDec = [];

        for (const datum of cleaned1) {
            if (datum.date.split("-")[1] === "12") {
                crimeInDec.push(datum);
            }
        }

        for (const datum of cleaned2) {
            if (datum.date2.split("-")[1] === "01") {
                crimeInJan.push(datum);
            }
        }

        const filtered = [];

        for (const datum1 of crimeInDec) {
            for (const datum2 of crimeInJan) {
                if (datum1.area === datum2.area2) {
                    filtered.push({...datum1, ...datum2});
                }
            }
        }

        expect(out).to.deep.equal(filtered);
    });

    it("produces correct output for duplicateTuplesQuery", () => {
        const out = queries.duplicateTuplesQuery.execute(rawData);
        const joined = [];

        for (const datum1 of rawData) {
            for (const datum2 of rawData) {
                joined.push(datum2);
            }
        }

        expect(out).to.deep.equal(joined);
    });

    it("produces correct output for duplicateTuplesQuery", () => {
        const out = queries.duplicateTuplesQuery.execute(rawData);
        const joined = [];

        for (const datum1 of rawData) {
            for (const datum2 of rawData) {
                joined.push(datum2);
            }
        }

        expect(out).to.deep.equal(joined);
    });


    it("Test Joins on Area", () => {
        const query = Q.join(Q, "area");
        const out = query.execute(cleanData());

        let good = 0;
        for (const datum1 of rawData) {
            for (const datum2 of rawData) {
                if (datum1[4] === datum2[4]) {
                    good++;
                }
            }
        }

        expect(out).to.be.an("Array");
        expect(out).to.have.length(good);
    });

    it("Optimize hashjoin on Area", () => {
        const query = Q.join(Q, "area");

        const opt = query.optimize();
        expectQuerySequence(opt, ["HashJoin"]);

        const out = opt.execute(rawData);

        expect(out).to.be.an("Array");
        expect(out).to.deep.equal(query.execute(rawData));
    });

    it("supports sorting", () => {
        const data = [
            {a: 1, b: 2},
            {a: 3, b: 3},
            {a: 5, b: 6},
            {a: 2, b: 1},
            {a: 4, b: 3},
        ];

        const expected = [
            {a: 1, b: 2},
            {a: 2, b: 1},
            {a: 3, b: 3},
            {a: 4, b: 3},
            {a: 5, b: 6},
        ];

        expect(Q.sort((a, b) => a.a - b.a).execute(data)).to.deep.equal(expected);
    });

    it("supports reducing", () => {
        const data = [
            {a: 1, b: 2},
            {a: 3, b: 3},
            {a: 5, b: 6},
            {a: 2, b: 1},
            {a: 4, b: 3},
        ];

        const expected = [
            {a: 16, b: 15},
        ];

        expect(Q.reduce((a, b) => ({a: a.a + b.a, b: a.b + b.b}), {a: 1, b: 0}).execute(data)).to.deep.equal(expected);
    });
});
