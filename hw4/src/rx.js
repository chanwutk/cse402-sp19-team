class Stream {
    constructor() {
        this.callbacks = [];
    }

    subscribe(callback) {
        if (typeof callback !== "function") {
            throw new Error("callback must be a function");
        }
        this.callbacks.push(callback);
        return this;
    }

    _push(param) {
        this.callbacks.forEach((callback) => callback(param));
    }

    // eslint-disable-next-line camelcase
    _push_many(params) {
        params.forEach((param) => this._push(param));
    }

    first() {
        const out = new Stream();
        let first = true;
        this.subscribe((param) => {
            if (first) {
                out._push(param);
                first = false;
            }
        });
        return out;
    }

    map(callback) {
        const out = new Stream();
        this.subscribe((param) => out._push(callback(param)));
        return out;
    }

    filter(callback) {
        const out = new Stream();
        this.subscribe((param) => callback(param) ? out._push(param) : undefined);
        return out;
    }

    distinctUntilChanged() {
        const out = new Stream();
        let prev;
        let isFirst = true;
        this.subscribe((param) => {
            if (isFirst || param !== prev) {
                out._push(param);
            }
            isFirst = false;
            prev = param;
        });
        return out;
    }

    flatten() {
        const out = new Stream();
        this.subscribe((param) => {
            (Array.isArray(param) ? param : [param]).forEach((p) => out._push(p));
        });
        return out;
    }

    scan(callback, acc) {
        let prev = acc;
        const out = new Stream();
        this.subscribe((param) => {
            const output = callback(prev, param);
            out._push(callback(prev, param));
            prev = output;
        });
        return out;
    }

    join(other) {
        other.subscribe((p) => this._push(p));
        return this;
    }

    combine() {
        const out = new Stream();
        this.subscribe((stream) => {
            stream.subscribe((param) => {
                out._push(param);
            });
        });
        return out;
    }

    zip(other, callback) {
        const out = new Stream();
        this.subscribe((p) => {
            out.thisInput = p;
            if ("otherInput" in out) {
                out._push(callback(out.thisInput, out.otherInput));
            }
        });
        other.subscribe((p) => {
            out.otherInput = p;
            if ("thisInput" in out) {
                out._push(callback(out.thisInput, out.otherInput));
            }
        });
        return out;
    }

    throttle(N) {
        const out = new Stream();
        let pushCount = 0;
        this.subscribe((param) => {
            const currentPush = ++pushCount;
            setTimeout(() => {
                if (currentPush === pushCount) {
                    out._push(param);
                }
            }, N);
        });
        return out;
    }

    latest() {
        const out = new Stream();
        let streamCounter = 0;
        let currentStream = -1;
        this.subscribe((stream) => {
            const streamId = streamCounter++;
            stream.subscribe((param) => {
                if (streamId >= currentStream) {
                    currentStream = streamId;
                    out._push(param);
                }
            });
        });
        return out;
    }

    unique(f) {
        const pastValues = new Set();
        const out = new Stream();
        this.subscribe((param) => {
            const key = f(param);
            if (!pastValues.has(key)) {
                out._push(param);
                pastValues.add(key);
            }
        });
        return out;
    }

    static timer(N) {
        const out = new Stream();
        setInterval(() => out._push(new Date()), N);
        return out;
    }

    static dom(element, eventname) {
        const out = new Stream();
        element.on(eventname, (event) => out._push(event));
        return out;
    }

    static url(url) {
        const out = new Stream();
        $.get(url, (json) => {
            out._push(json);
        }, "json");
        return out;
    }

    // *** NEW ***
    static ajax(_url, _data) {
        const out = new Stream();
        $.ajax({
            url: _url,
            dataType: "jsonp",
            jsonp: "callback",
            data: _data,
            success: (data) => {
                out._push(data[1]);
            },
        });
        return out;
    }
}

if (typeof exports !== "undefined") {
    exports.Stream = Stream;
}

// dependency injection let's do it
function setup($) {
    const FIRE911URL = "https://data.seattle.gov/views/kzjm-xkqj/rows.json?accessType=WEBSITE&method=getByIds&asHashes=true&start=0&length=10&meta=false&$order=:id";

    // *** NEW ***
    function WIKIPEDIAGET(s) {
        return Stream.ajax(
            "https://en.wikipedia.org/w/api.php",
            {
                action: "opensearch",
                search: s,
                namespace: 0,
                limit: 10,
            }
        );
    }

    // Add your hooks to implement Parts 2-4 here.
    const timer = Stream.timer(100);
    timer.subscribe((date) => $("#time").text(date.toString()));

    const click = Stream.dom($("#button"), "click");
    let numClick = 0;
    const addClick = () => numClick++;
    $("#clicks").text(addClick());
    click.subscribe((event) => {
        if (event && event.type === "click" && event.target && event.target.id === "button") {
            $("#clicks").text(addClick());
        }
    });

    const mousePos = Stream.dom($("#mousemove"), "mousemove");
    const throttled = mousePos.throttle(1000);
    throttled.subscribe((event) => {
        if (event && event.originalEvent && event.target) {
            const { originalEvent, target } = event;
            const x = originalEvent.clientX - target.offsetLeft;
            const y = originalEvent.clientY - target.offsetTop;
            $("#mouseposition").text(`(${x}, ${y})`);
        }
    });

    const getFireInfo = () => Stream.url(FIRE911URL);
    const fireEvents = [];
    const fireEventsStream = Stream.timer(1000 * 60).map(getFireInfo).combine();
    const firstEventStream = Stream.timer(100).map(getFireInfo).first().combine();
    fireEventsStream.join(firstEventStream).flatten().unique((obj) => obj.id).subscribe((event) => {
        fireEvents.push(event);
    });

    const fireEventsProducer = Stream.timer(50).map(() => fireEvents);

    const fireSearch = Stream.dom($("#firesearch"), "input").map((event) => {
        const value = event.target.value;
        $("#fireevents").html("");
        if (value === "") {
            return fireEventsProducer.first().flatten();
        } else {
            return fireEventsProducer.first().flatten().filter((fireEvent) => fireEvent[405818852].includes(value));
        }
    }).latest();

    fireSearch.subscribe((event) => {
        $("#fireevents").append(`<li>
            <b>${event[405818853]}</b> at ${event[405818852]}
            </br>
            ${new Date(event.updated_at)}
        </li>`);
    });

    const wikiSuggestion = Stream.dom($("#wikipediasearch"), "input").map((event) => {
        const value = event.target.value;
        if (value === "") {
            $("#wikipediasuggestions").html("");
            return Stream.timer(100).map((_) => []).first();
        } else {
            return WIKIPEDIAGET(value);
        }
    }).latest();

    wikiSuggestion.subscribe((suggestions) => {
        $("#wikipediasuggestions").html("");
        suggestions.forEach((suggestion) => $("#wikipediasuggestions").append(`<li>${suggestion}</li>`));
    });

    let maxLoad = 10;
    let currentLoad = 0;
    const loadCompleteStream = Stream.dom($("#load-complete"), "click").map(() => {
        currentLoad = maxLoad;
        $("#progress").addClass("finished");
    });
    const loadMoreStream = Stream.dom($("#load-more"), "click").map(() => {
        if (currentLoad < maxLoad) {
            currentLoad += 1;
            if (currentLoad === maxLoad) {
                maxLoad *= 2;
            }
        }
    });
    loadMoreStream.join(loadCompleteStream).subscribe(() => $("#progress").css("width", `${currentLoad * 100 / maxLoad}%`));
}
